# SkyPrice Search Engine

SkyPrice Search Engine is a travel search engine consisting of a results page for their flight search functionality. There is a form for the user can input filter and search for the flights. Then, the user will be able to see the search results, and refine the search using price and airlines.

## Getting Started

These instructions will show you how to run this project on your local machine for development and for production build.

### Installing

Firstly, clone this repository and then install it:

```
git clone https://gitlab.com/janewee/skyprice.git
cd skyprice
npm install
```

Once done, to run the project for production build on your local machine:

```
npm run build
npm install -g serve
serve -s build
```
After that, you can access it through `http://localhost:5000`

Alternatively, if you want to run for development purposes:
```
npm start
```

## Built With

* [React](https://reactjs.org/) - The web front end framework used (JSX, CSS, HTML)
* [reactstrap](https://reactstrap.github.io/) - React Bootstrap 4 components
* [react-input-range](https://github.com/davidchin/react-input-range) - Used for dual input range slider
* [API](http://www.mocky.io/v2/5b5df09532000093031cfa97) - mocky endpoint used to fetch and display results

## Design Overview

The design layout were based from the wireframe given. This project design was made to be simple and user-friendly. 

### Return Initial View

Upon entering the site, the user will view the list of available flights in SkySearch Search Engine.

![Return Default View](img/ReturnDefault.png)

### Return After Filter View

The user can search for their own flights by providing the search details. The user can proceed to refine their flights through price range selection and through airlines.

![Return Filter View](img/ReturnFilter.png)

### OneWay After Filter View

For users interested in OneWay flights, there is also a One Way search filter.

![One Way Filter View](img/OneWayFilter.png)

### Assumptions

* Prices are the same for OneWay or Return flights since the given Api data provides one price per flight. This makes return flights more worth to buy.
* OneWay flights display departure flights using departure dates Api data.
* User will search first and then only refines flight search with price and airlines.
* Maximum passengers total per flight search is 8 in total.
* "Your Results" detail will be displayed at the top only after a search.
* The flight search engine does not use total passengers to filter since no Api data for passengers in flight.

## Demo

The following are demo of SkyPrice Search Engine:

![Return Filter](img/Return.webm)

![One Way Filter](img/OneWay.webm)


