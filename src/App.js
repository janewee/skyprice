import React from 'react';
import './css/App.css';
import Header from './components/Header';
import FlightFilter from './components/FlightFilter';
import FlightResult from './components/FlightResult';
import {
  Container,
  Row,
  Col,
} from 'reactstrap';

//Splitting the pane into filter and results
function SplitPane(props) {
  return (
    <Container>
      <Row>
        <Col xs="3" className="SplitPane-right">{props.left}</Col>
        <Col xs="9" className="SplitPane-left">{props.right}</Col>
      </Row>
    </Container>
  );
}

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      from: '',
      destination: '',
      depart_date: '',
      return_date: '',
      passengers: '',
      resultFrom: '',
      resultDestination: '',
      resultDepartDate: '',
      resultReturnDate: '',
      resultPassengers: '',
      showResult: false,
      value: { min: 1, max: 125 },
      AI: true,
      TZ: true,
      MD: true,
      activeTab: '2',
      firstTime: true
    };

    this.toggle = this.toggle.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleInputRange = this.handleInputRange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.type === 'checkbox' ? e.target.checked : e.target.value
    })
  }

  handleInputRange(e) {
    this.setState({
      value: e
    })
  }

  onSubmit(e) {
    e.preventDefault();
    this.setState({
      resultFrom: this.state.from,
      resultDestination: this.state.destination,
      resultDepartDate: this.state.depart_date,
      resultReturnDate: this.state.return_date,
      resultPassengers: this.state.passengers,
      showResult: true,
      firstTime: false
    })
  }

  render() {
    return (
      <div>
        <Header />
        <SplitPane
          left={
            <FlightFilter
              filterFrom={this.state.from}
              filterDestination={this.state.destination}
              filterDepartDate={this.state.depart_date}
              filterReturnDate={this.state.return_date}
              filterPassengerTotal={this.state.passengers}
              handleChange={this.handleChange}
              onSubmit={this.onSubmit}
              filterPrice={this.state.value}
              handleInputRange={this.handleInputRange}
              AI={this.state.AI}
              TZ={this.state.TZ}
              MD={this.state.MD}
              activeTab={this.state.activeTab}
              handleToggle={this.toggle}
            />}
          right={
            <FlightResult
              filterFrom={this.state.resultFrom}
              filterDestination={this.state.resultDestination}
              filterDepartDate={this.state.resultDepartDate}
              filterReturnDate={this.state.resultReturnDate}
              filterPassengerTotal={this.state.resultPassengers}
              showResult={this.state.showResult}
              filterPrice={this.state.value}
              AI={this.state.AI}
              TZ={this.state.TZ}
              MD={this.state.MD}
              activeTab={this.state.activeTab}
              firstTime={this.state.firstTime}
            />}
        />
      </div>
    );
  }
}

export default App;
