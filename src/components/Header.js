import React from 'react';
import '../css/App.css';
import {
  Navbar,
  NavbarBrand, 
  } from 'reactstrap';

//navbar
class Header extends React.Component {
    render() {
      return (
         <Navbar className="App-header fixed-top">
          <NavbarBrand className="App-title" href="/">SkyPrice Search Engine</NavbarBrand>
        </Navbar>
      );
    }
  }

  export default Header;