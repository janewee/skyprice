import React from 'react';
import '../css/FlightFilter.css';
import {
    Container, Row,
    Button, Form, FormGroup, Label, Input
} from 'reactstrap';
import { TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap';
import classnames from 'classnames';
import InputRange from 'react-input-range';


//This is for controlling the form tabs to select 'OneWay flight' or 'Return flight' 
class Tabs extends React.Component {
    render() {
        return (
            <div>
                <Nav tabs className="tab">
                    <NavItem className="item item-left">
                        <NavLink
                            className={classnames({ active: this.props.activeTab === '1' })}
                            onClick={() => { this.props.toggle('1'); }}
                        >
                            One Way
                  </NavLink>
                    </NavItem>
                    <NavItem className="item">
                        <NavLink
                            className={classnames({ active: this.props.activeTab === '2' })}
                            onClick={() => { this.props.toggle('2'); }}
                        >
                            Return
                  </NavLink>
                    </NavItem>
                </Nav>
                <TabContent activeTab={this.props.activeTab}>
                    <TabPane tabId="1">
                        <SearchBar
                            handleChange={this.props.handleChange}
                            onSubmit={this.props.onSubmit}
                            filterFrom={this.props.filterFrom}
                            filterDestination={this.props.filterDestination}
                            filterDepartDate={this.props.filterDepartDate}
                            filterPassengerTotal={this.props.filterPassengerTotal}
                            tripType={this.props.activeTab}
                        />
                    </TabPane>
                    <TabPane tabId="2">
                        <SearchBar
                            handleChange={this.props.handleChange}
                            onSubmit={this.props.onSubmit}
                            filterFrom={this.props.filterFrom}
                            filterDestination={this.props.filterDestination}
                            filterDepartDate={this.props.filterDepartDate}
                            filterReturnDate={this.props.filterReturnDate}
                            filterPassengerTotal={this.props.filterPassengerTotal}
                            tripType={this.props.activeTab}
                        />
                    </TabPane>
                </TabContent>
            </div>
        );
    }
}

//This is for the Form Search filters
class SearchBar extends React.Component {

    render() {

        const isReturn = this.props.tripType;

        return (
            <Form className="form-bg">
                <FormGroup className="group-margin">
                    <Label for="fromSearch">From</Label>
                    <Input type="search" name="from" id="fromSearch" placeholder="Enter city/airport" value={this.props.filterFrom} onChange={this.props.handleChange} />
                </FormGroup>
                <FormGroup className="group-margin">
                    <Label for="destinationSearch">Destination</Label>
                    <Input type="search" name="destination" id="destinationSearch" placeholder="Enter city/airport" value={this.props.filterDestination} onChange={this.props.handleChange} />
                </FormGroup>
                <FormGroup className="group-margin">
                    <Label for="departureDate">Departure Date</Label>
                    <Input type="date" name="depart_date" id="departureDate" placeholder="date placeholder" value={this.props.filterDepartDate} onChange={this.props.handleChange} />
                </FormGroup>
                {isReturn == 2 &&
                    <FormGroup className="group-margin">
                        <Label for="returnDate">Return Date</Label>
                        <Input type="date" name="return_date" id="returnDate" placeholder="date placeholder" value={this.props.filterReturnDate} onChange={this.props.handleChange} />
                    </FormGroup>
                }
                <FormGroup className="group-margin">
                    <Label for="passengersSelect">Passengers</Label>
                    <PassengerTotal numbers={numbers} value={this.props.filterPassengerTotal} handleChange={this.props.handleChange} />
                </FormGroup>
                <Button className="align" onClick={this.props.onSubmit}>Search</Button>
            </Form>
        );
    }
}

//This is for RefineSearch filters
class RefineSearch extends React.Component {

    render() {
        return (
            <div className="refine">
                <Form>
                    <Label for="refineSearch" className="price">Refine Flight Search</Label>
                    <div className="input-range">
                        <InputRange
                            formatLabel={value => `${'£' + value}`}
                            maxValue={200}
                            minValue={0}
                            value={this.props.filterPrice}
                            onChange={this.props.handleInputRange}
                            name="value"
                        />
                    </div>
                    <Label for="refineSearch" className="price">Airlines</Label>
                    <FormGroup check>
                        <Input type="checkbox" className="airline-choice" checked={this.props.AI} onChange={this.props.handleChange} name="AI" />{' '}
                        <Label for="checkbox" className="airline-text">AI</Label>
                        <Input type="checkbox" className="airline-choice" checked={this.props.TZ} onChange={this.props.handleChange} name="TZ" />{' '}
                        <Label for="checkbox" className="airline-text">TZ</Label>
                        <Input type="checkbox" className="airline-choice" checked={this.props.MD} onChange={this.props.handleChange} name="MD" />{' '}
                        <Label for="checkbox" className="airline-text">MD</Label>
                    </FormGroup>
                </Form>
            </div>
        );
    }
}

//Choices for passenger total in form
const numbers = [1, 2, 3, 4, 5, 6, 7, 8];

function OptionItems(props) {
    return <option>{props.value}</option>;
}

//Display Max Passenger Total available in form selection
function PassengerTotal(props) {
    const numbers = props.numbers;
    const listItems = numbers.map((number) =>
        <OptionItems key={number.toString()}
            value={number} />
    );
    return (
        <Input type="select" name="passengers" id="passengersSelect" onChange={props.handleChange} >
            {listItems}
        </Input>
    );
}

//This class controls the flight filters for search and refine search
class FlightFilter extends React.Component {
    render() {
        return (
            <Container >
                <Row className="row-margin" >
                    <Tabs
                        handleChange={this.props.handleChange}
                        onSubmit={this.props.onSubmit}
                        filterFrom={this.props.filterFrom}
                        filterDestination={this.props.filterDestination}
                        filterDepartDate={this.props.filterDepartDate}
                        filterReturnDate={this.props.filterReturnDate}
                        filterPassengerTotal={this.props.filterPassengerTotal}
                        activeTab={this.props.activeTab}
                        toggle={this.props.handleToggle}
                    />
                </Row>
                <Row className="row-margin">
                    <RefineSearch
                        handleInputRange={this.props.handleInputRange}
                        filterPrice={this.props.filterPrice}
                        AI={this.props.AI}
                        TZ={this.props.TZ}
                        MD={this.props.MD}
                        handleChange={this.props.handleChange}
                    />
                </Row>
            </Container>
        );
    }
}

export default FlightFilter;