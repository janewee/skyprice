import React from 'react';
import '../css/FlightResult.css';
import {
  ListGroup, ListGroupItem, Button, Container, Row, Col,
} from 'reactstrap';
import sgp_logo from '../assets/singapore_airlines.svg';

//shows the result for depart and return info on top of flight results
class ResultHeader extends React.Component {
  render() {

    const depart_from = this.props.filterFrom;
    const arrive_at = this.props.filterDestination;
    const showResult = this.props.showResult;
    const activeTab = this.props.activeTab;

    if (showResult) {
      return (
        <div className="result-container">
          <p className="title">Your Results</p>
          {}
          <div>
            <p className="sub-title">Depart : {depart_from + " ------> "}{arrive_at}</p>
            {activeTab === "2" ? (
              <p className="sub-title"> Return : {arrive_at + " ------> "}{depart_from}</p>
            ) : ''}
          </div>
        </div>
      );
    }
    return ('');
  }
}


function ListItem(props) {

  const activeTab = props.activeTab;

  return (
    <ListGroupItem className="list-group">
      <Container className="container-margin">
        <Row>
          <Col>
            <img src={sgp_logo} alt="" className="img-item" />
          </Col>
          <Col className="col-align">
            <Row>
              <p className="flight-no">{props.depart_flight_no}</p>
            </Row>
            <Row>
              <p className="flight-travel">{props.depart_flight_name}</p>
            </Row>
            <Row>
              <p className="flight-time">Depart: {props.depart_depart_time}</p>
            </Row>
            <Row>
              <p className="flight-time">Arrive:&nbsp;&nbsp; {props.depart_arrive_time}</p>
            </Row>
          </Col>
          {activeTab === "2" ? (
            <Col>
              <Row>
                <p className="flight-no">{props.arriv_flight_no}</p>
              </Row>
              <Row>
                <p className="flight-travel">{props.arriv_flight_name}</p>
              </Row>
              <Row>
                <p className="flight-time">Depart: {props.arriv_depart_time}</p>
              </Row>
              <Row>
                <p className="flight-time">Arrive:&nbsp;&nbsp; {props.arriv_arrive_time}</p>
              </Row>
            </Col>
          ) : (
              <Col>
                <Row></Row>
                <Row></Row>
                <Row></Row>
                <Row></Row>
              </Col>
            )}
          <Col><div className="price-align">£{props.price}</div></Col>
          <Col className="btn-align">
            <Button className="btn-properties">Select this flight</Button></Col>
        </Row>
      </Container>
    </ListGroupItem>
  );
}

//This displays the search results
class SearchResult extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: [],
    };
  }

  //fetching the mocky endpoint data
  componentDidMount() {
    fetch("http://www.mocky.io/v2/5b5df09532000093031cfa97")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            items: result.data
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  //filtering the results to be displayed based on search and refine search
  filterResults(results) {
    let fromFilter = this.props.filterFrom;
    let destination = this.props.filterDestination;
    let departDate = this.props.filterDepartDate;
    let returnDate = this.props.filterReturnDate;
    let filterPrice = this.props.filterPrice;
    let AI = this.props.AI;
    let TZ = this.props.TZ;
    let MD = this.props.MD;
    let activeTab = this.props.activeTab;

    return results.filter((result) => {
      if (this.extractFromFlight(result.departure_flight.flight_name) !== fromFilter) {
        return false;
      }
      if (this.extractToFlight(result.departure_flight.flight_name) !== destination) {
        return false;
      }
      if (this.extractDate(result.departure_flight.date) !== departDate) {
        return false;
      }
      if ((activeTab === '2') && this.extractDate(result.arrival_flight.date) !== returnDate) {
        return false;
      }
      if (this.extractPrice(result.price) < filterPrice.min || this.extractPrice(result.price) > filterPrice.max) {
        return false;
      }

      let isExist = false;

      if (AI) {
        if (this.extractAirlines(result.departure_flight.flight_no) === "AI") {
          isExist = true;
        }
      } else {
        if (this.extractAirlines(result.departure_flight.flight_no) === "AI") {
          return false;
        }
      }

      if (TZ) {
        if (this.extractAirlines(result.departure_flight.flight_no) === "TZ") {
          isExist = true;
        }
      } else {
        if (this.extractAirlines(result.departure_flight.flight_no) === "TZ") {
          return false;
        }
      }

      if (MD) {
        if (this.extractAirlines(result.departure_flight.flight_no) === "MD") {
          isExist = true;
        }
      } else {
        if (this.extractAirlines(result.departure_flight.flight_no) === "MD") {
          return false;
        }
      }

      if (!isExist) {
        return false;
      }

      return true;
    });
  }

  //retrieve airlines from API
  extractAirlines(flight_name) {
    return flight_name.substr(0, 2);
  }

  extractFromFlight(flight_name) {
    return flight_name.substr(0, 3);
  }

  extractToFlight(flight_name) {
    return flight_name.substr(6, 3);
  }

  extractDate(flight_date) {
    return flight_date.substr(0, 10);
  }

  extractPrice(flight_price) {
    return parseInt(flight_price);
  }

  render() {
    let { error, isLoaded, items } = this.state;

    if (!this.props.firstTime) {
      items = this.filterResults(items);
    }

    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <ListGroup>
          {items.map(item => (
            <ListItem key={item.departure_flight.flight_no}
              depart_flight_no={item.departure_flight.flight_no}
              depart_flight_name={item.departure_flight.flight_name}
              depart_depart_time={item.departure_flight.depart_time}
              depart_arrive_time={item.departure_flight.arrive_time}
              depart_date={item.departure_flight.date}

              arriv_flight_no={item.arrival_flight.flight_no}
              arriv_flight_name={item.arrival_flight.flight_name}
              arriv_depart_time={item.arrival_flight.depart_time}
              arriv_arrive_time={item.arrival_flight.arrive_time}
              arriv_date={item.arrival_flight.date}

              price={item.price}
              activeTab={this.props.activeTab}
            />
          ))}
        </ListGroup>
      );
    }
  }
}

//this controls the results shown from the API and search filter
class FlightResult extends React.Component {
  render() {
    return (
      <div>
        <ResultHeader
          filterFrom={this.props.filterFrom}
          filterDestination={this.props.filterDestination}
          showResult={this.props.showResult}
          activeTab={this.props.activeTab}
        />
        <SearchResult
          filterFrom={this.props.filterFrom}
          filterDestination={this.props.filterDestination}
          filterDepartDate={this.props.filterDepartDate}
          filterReturnDate={this.props.filterReturnDate}
          filterPassengerTotal={this.props.filterPassengerTotal}
          filterPrice={this.props.filterPrice}
          AI={this.props.AI}
          TZ={this.props.TZ}
          MD={this.props.MD}
          activeTab={this.props.activeTab}
          firstTime={this.props.firstTime}
        />
      </div>
    );
  }
}

export default FlightResult;